FROM ubuntu
COPY helloworld.go app.go
RUN apt-get update
RUN apt-get install -y wget
RUN wget https://golang.org/dl/go1.17.1.linux-amd64.tar.gz
RUN rm -rf /usr/local/go && tar -C /usr/local -xzf go1.17.1.linux-amd64.tar.gz
ENV PATH=$PATH:/usr/local/go/bin
CMD go run app.go
